/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.ConsultaInicial;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author estudiante
 */
public class ConsultaInicialDao implements ConsultaInicialServices {

    public static final String SQL_INSERT = "INSERT INTO consulta_inicial(id, fecha, sintomas, id_paciente, diagnostico_inicial) VALUES (?,?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM consulta_inicial";
    public static final String SQL_CONSULTAID = "SELECT * FROM consulta_inicial WHERE id = ?";
    public static final String SQL_DELETE = "DELETE FROM consulta_inicial WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE consulta_inicial SET fecha = ?, sintomas = ?, id_paciente = ?, diagnostico_inicial = ?  WHERE id = ?";

    @Override
    public int create(Modelo.Entity.ConsultaInicial consultaInicial) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, consultaInicial.getId());
            ps.setString(2, consultaInicial.getFecha());
            ps.setString(3, consultaInicial.getSintomas());
            ps.setString(4, consultaInicial.getIdPaciente());
            ps.setString(5, consultaInicial.getDiagnosticoInicial());
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Modelo.Entity.ConsultaInicial> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<ConsultaInicial> consultasIniciales = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                String fecha = res.getString("fecha");
                String sintomas = res.getString("sintomas");
                String idPaciente = res.getString("id_paciente");
                String diagnosticoInicial = res.getString("diagnostico_inicial");
                ConsultaInicial consultasInicial = new ConsultaInicial(diagnosticoInicial, id, fecha, sintomas, idPaciente);
                consultasIniciales.add(consultasInicial);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return consultasIniciales;
    }

    @Override
    public Modelo.Entity.ConsultaInicial selectId(Modelo.Entity.ConsultaInicial consultaInicial) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        ConsultaInicial registroConsultaInicial = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, consultaInicial.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            String fecha = res.getString("fecha");
            String sintomas = res.getString("sintomas");
            String idPaciente = res.getString("id_paciente");
            String diagnosticoInicial = res.getString("diagnostico_inicial");
            registroConsultaInicial = new ConsultaInicial(diagnosticoInicial, id, fecha, sintomas, idPaciente);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroConsultaInicial;
    }

    @Override
    public int update(Modelo.Entity.ConsultaInicial consultaInicial) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);     
            ps.setString(1, consultaInicial.getFecha());
            ps.setString(2, consultaInicial.getSintomas());
            ps.setString(3, consultaInicial.getIdPaciente());
            ps.setString(4, consultaInicial.getDiagnosticoInicial());
            ps.setInt(5, consultaInicial.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Modelo.Entity.ConsultaInicial consultaInicial) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, consultaInicial.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
