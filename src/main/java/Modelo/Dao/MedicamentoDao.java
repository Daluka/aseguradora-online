/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.Medicamento;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author estudiante
 */
public class MedicamentoDao implements MedicamentoServices {

    public static final String SQL_INSERT = "INSERT INTO medicamento(id, nombre, efecto_secundario, beneficios) VALUES (?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM medicamento";
    public static final String SQL_CONSULTAID = "SELECT * FROM medicamento WHERE id = ?";
    public static final String SQL_DELETE = "DELETE FROM medicamento WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE medicamento SET nombre = ?,  efecto_secundario = ?, beneficios = ? WHERE id = ?";

    @Override
    public int create(Modelo.Entity.Medicamento medicamento) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, medicamento.getId());
            ps.setString(2, medicamento.getNombre());
            ps.setString(3, medicamento.getEfectoSecundario());
            ps.setString(4, medicamento.getBeneficios());
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Modelo.Entity.Medicamento> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Medicamento> medicamentos = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                String nombre = res.getString("nombre");
                String efectoSecundario = res.getString("efecto_secundario");
                String beneficios = res.getString("beneficios");
                Medicamento medicamento = new Medicamento(id, nombre, efectoSecundario, beneficios);
                medicamentos.add(medicamento);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return medicamentos;
    }

    @Override
    public Modelo.Entity.Medicamento selectId(Modelo.Entity.Medicamento medicamento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Medicamento registroMedicamento = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, medicamento.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            String nombre = res.getString("nombre");
            String efectoSecundario = res.getString("efecto_secundario");
            String beneficios = res.getString("beneficios");
            registroMedicamento = new Medicamento(id, nombre, efectoSecundario, beneficios);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroMedicamento;
    }

    @Override
    public int update(Modelo.Entity.Medicamento medicamento) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setInt(4, medicamento.getId());
            ps.setString(1, medicamento.getNombre());
            ps.setString(2, medicamento.getEfectoSecundario());
            ps.setString(3, medicamento.getBeneficios());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Modelo.Entity.Medicamento medicamento) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, medicamento.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
