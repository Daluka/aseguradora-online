/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.ConsultaSeguimiento;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author estudiante
 */
public class ConsultaSeguimientoDao implements ConsultaSeguimientoServices {

    public static final String SQL_INSERT = "INSERT INTO consulta_seguimiento(id, fecha, sintomas, id_paciente, estado_diagnostico) VALUES (?,?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM consulta_seguimiento";
    public static final String SQL_CONSULTAID = "SELECT * FROM consulta_seguimiento WHERE id = ?";
    public static final String SQL_DELETE = "DELETE FROM consulta_seguimiento WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE consulta_seguimiento SET fecha = ?, sintomas = ?, id_paciente = ?, estado_diagnostico = ?  WHERE id = ?";

    @Override
    public int create(Modelo.Entity.ConsultaSeguimiento consultaSeguimiento) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, consultaSeguimiento.getId());
            ps.setString(2, consultaSeguimiento.getFecha());
            ps.setString(3, consultaSeguimiento.getSintomas());
            ps.setString(4, consultaSeguimiento.getIdPaciente());
            ps.setString(5, consultaSeguimiento.getEstadoDiagnostico());
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Modelo.Entity.ConsultaSeguimiento> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<ConsultaSeguimiento> consultasSeguimiento = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                String fecha = res.getString("fecha");
                String sintomas = res.getString("sintomas");
                String idPaciente = res.getString("id_paciente");
                String estadoDiagnostico = res.getString("estado_diagnostico");
                ConsultaSeguimiento consultaSeguimiento = new ConsultaSeguimiento(estadoDiagnostico, id, fecha, sintomas, idPaciente);
                consultasSeguimiento.add(consultaSeguimiento);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return consultasSeguimiento;
    }

    @Override
    public Modelo.Entity.ConsultaSeguimiento selectId(Modelo.Entity.ConsultaSeguimiento consultaSeguimiento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        ConsultaSeguimiento registroConsultaSeguimiento = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, consultaSeguimiento.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            String fecha = res.getString("fecha");
            String sintomas = res.getString("sintomas");
            String idPaciente = res.getString("id_paciente");
            String estadoDiagnostico = res.getString("estado_diagnostico");
            registroConsultaSeguimiento = new ConsultaSeguimiento(estadoDiagnostico, id, fecha, sintomas, idPaciente);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroConsultaSeguimiento;
    }

    @Override
    public int update(Modelo.Entity.ConsultaSeguimiento consultaSeguimiento) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(1, consultaSeguimiento.getFecha());
            ps.setString(2, consultaSeguimiento.getSintomas());
            ps.setString(3, consultaSeguimiento.getIdPaciente());
            ps.setString(4, consultaSeguimiento.getEstadoDiagnostico());
            ps.setInt(5, consultaSeguimiento.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Modelo.Entity.ConsultaSeguimiento consultaSeguimiento) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, consultaSeguimiento.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
