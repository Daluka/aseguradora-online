/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.Seguro;
import java.util.List;

/**
 *
 * @author estudiante
 */
public interface SeguroServices {

    public int create(Seguro seguro);
    public List<Seguro> all();
    public Seguro selectId(Seguro seguro);
    public int update(Seguro seguro);
    public int delete(Seguro seguro);
            
}
