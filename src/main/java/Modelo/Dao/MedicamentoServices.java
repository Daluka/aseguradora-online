/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.Medicamento;
import java.util.List;

/**
 *
 * @author estudiante
 */
public interface MedicamentoServices {

    public int create(Medicamento medicamento);
    public List<Medicamento> all();
    public Medicamento selectId(Medicamento medicamento);
    public int update(Medicamento medicamento);
    public int delete(Medicamento medicamento);
            
}
