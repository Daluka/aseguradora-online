/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.Doctor;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author estudiante
 */
public class DoctorDao implements DoctorServices {

    public static final String SQL_INSERT = "INSERT INTO doctor(id, nombre, apellido, direccion, telefono, correo_electronico, especializacion) VALUES (?,?,?,?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM doctor";
    public static final String SQL_CONSULTAID = "SELECT * FROM doctor WHERE id = ?";
    public static final String SQL_DELETE = "DELETE FROM doctor WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE doctor SET nombre = ?, apellido = ?, direccion = ?, telefono = ?, correo_electronico = ?, especializacion = ? WHERE id = ?";

    @Override
    public int create(Modelo.Entity.Doctor doctor) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setString(1, doctor.getId());
            ps.setString(2, doctor.getNombre());
            ps.setString(3, doctor.getApellido());
            ps.setString(4, doctor.getDireccion());
            ps.setString(5, doctor.getTelefono());
            ps.setString(6, doctor.getCorreoElectronico());
            ps.setString(7, doctor.getEspecializacion());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Modelo.Entity.Doctor> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Doctor> doctores = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                String id = res.getString("id");
                String nombre = res.getString("nombre");
                String apellido = res.getString("apellido");
                String direccion = res.getString("direccion");
                String telefono = res.getString("telefono");
                String correoElectronico = res.getString("correo_electronico");
                String especializacion = res.getString("especializacion");
                Doctor doctor = new Doctor(especializacion, id, id, nombre, apellido, direccion, telefono, correoElectronico);
                doctores.add(doctor);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return doctores;
    }

    @Override
    public Modelo.Entity.Doctor selectId(Modelo.Entity.Doctor doctor) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Doctor registroDoctor = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setString(1, doctor.getId());
            res = ps.executeQuery();
            res.absolute(1);
            String id = res.getString("id");
            String nombre = res.getString("nombre");
            String apellido = res.getString("apellido");
            String direccion = res.getString("direccion");
            String telefono = res.getString("telefono");
            String correoElectronico = res.getString("correo_electronico");
            String especializacion = res.getString("especializacion");
            registroDoctor = new Doctor(especializacion, id, id, nombre, apellido, direccion, telefono, correoElectronico);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroDoctor;
    }

    @Override
    public int update(Modelo.Entity.Doctor doctor) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(7, doctor.getId());
            ps.setString(1, doctor.getNombre());
            ps.setString(2, doctor.getApellido());
            ps.setString(3, doctor.getDireccion());
            ps.setString(4, doctor.getTelefono());
            ps.setString(5, doctor.getCorreoElectronico());
            ps.setString(6, doctor.getEspecializacion());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Modelo.Entity.Doctor doctor) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setString(1, doctor.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
