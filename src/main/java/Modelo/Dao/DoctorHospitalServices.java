/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.DoctorHospital;
import java.util.List;

/**
 *
 * @author Daniel
 */
public interface DoctorHospitalServices {
    
    public int create(DoctorHospital doctorHospital);
    public List<DoctorHospital> all();
    public int delete(DoctorHospital doctorHospital);
}
