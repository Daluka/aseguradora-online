/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.Seguro;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author estudiante
 */
public class SeguroDao implements SeguroServices {

    public static final String SQL_INSERT = "INSERT INTO seguro(id, nombre, telefono) VALUES (?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM seguro";
    public static final String SQL_CONSULTAID = "SELECT * FROM seguro WHERE id = ?";
    public static final String SQL_DELETE = "DELETE FROM seguro WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE seguro SET nombre = ?, telefono = ? WHERE id = ?";

    @Override
    public int create(Modelo.Entity.Seguro seguro) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setString(1, seguro.getId());
            ps.setString(2, seguro.getNombre());
            ps.setString(3, seguro.getTelefono());
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Modelo.Entity.Seguro> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Seguro> seguros = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                String id = res.getString("id");
                String nombre = res.getString("nombre");
                String telefono = res.getString("telefono");
                Seguro seguro = new Seguro(id, nombre, telefono);
                seguros.add(seguro);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return seguros;
    }

    @Override
    public Modelo.Entity.Seguro selectId(Modelo.Entity.Seguro seguro) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Seguro registroSeguro = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setString(1, seguro.getId());
            res = ps.executeQuery();
            res.absolute(1);
            String id = res.getString("id");
            String nombre = res.getString("nombre");
            String telefono = res.getString("telefono");
            registroSeguro = new Seguro(id, nombre, telefono);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroSeguro;
    }

    @Override
    public int update(Modelo.Entity.Seguro seguro) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(1, seguro.getNombre());
            ps.setString(2, seguro.getTelefono());
            ps.setString(3, seguro.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Modelo.Entity.Seguro seguro) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setString(1, seguro.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
