/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.Paciente;
import java.util.List;

/**
 *
 * @author estudiante
 */
public interface PacienteServices {

    public int create(Paciente paciente);
    public List<Paciente> all();
    public Paciente selectId(Paciente paciente);
    public int update(Paciente paciente);
    public int delete(Paciente paciente);
            
}
