/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.DoctorHospital;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class DoctorHospitalDao implements DoctorHospitalServices {

    public static final String SQL_INSERT = "INSERT INTO doctor_hospital(id_doctor, id_hospital) VALUES (?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM doctor_hospital";
    public static final String SQL_DELETE = "DELETE FROM doctor_hospital WHERE id_doctor = ? AND id_hospital = ? ";

    @Override
    public int create(DoctorHospital doctorHospital) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setString(1, doctorHospital.getIdDoctor());
            ps.setString(2, doctorHospital.getIdHospital());
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<DoctorHospital> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<DoctorHospital> doctores = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                String idDoctor = res.getString("id_doctor");
                String idHospital = res.getString("id_hospital");
                DoctorHospital doctor = new DoctorHospital(idDoctor, idHospital);
                doctores.add(doctor);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return doctores;
    }

    @Override
    public int delete(DoctorHospital doctorHospital) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setString(1, doctorHospital.getIdDoctor());
            ps.setString(2, doctorHospital.getIdHospital());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
