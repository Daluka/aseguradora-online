/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.ConsultaRutinaria;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author estudiante
 */
public class ConsultaRutinariaDao implements ConsultaRutinariaServices {

    public static final String SQL_INSERT = "INSERT INTO consulta_rutinaria(id, fecha, sintomas, id_paciente, presion_arterial, estatura, peso, diagnostico) VALUES (?,?,?,?,?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM consulta_rutinaria";
    public static final String SQL_CONSULTAID = "SELECT * FROM consulta_rutinaria WHERE id = ?";
    public static final String SQL_DELETE = "DELETE FROM consulta_rutinaria WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE consulta_rutinaria SET fecha = ?, sintomas = ?, id_paciente = ?, presion_arterial = ?, estatura = ?, peso = ?, diagnostico = ?  WHERE id = ?";

    @Override
    public int create(Modelo.Entity.ConsultaRutinaria consultaRutinaria) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, consultaRutinaria.getId());
            ps.setString(2, consultaRutinaria.getFecha());
            ps.setString(3, consultaRutinaria.getSintomas());
            ps.setString(4, consultaRutinaria.getIdPaciente());
            ps.setString(5, consultaRutinaria.getPresionArterial());
            ps.setDouble(6, consultaRutinaria.getEstatura());
            ps.setDouble(7, consultaRutinaria.getPeso());
            ps.setString(8, consultaRutinaria.getDiagnostico());
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Modelo.Entity.ConsultaRutinaria> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<ConsultaRutinaria> consultasRutinarias = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                String fecha = res.getString("fecha");
                String sintomas = res.getString("sintomas");
                String idPaciente = res.getString("id_paciente");
                String presionArterial = res.getString("presion_arterial");
                Double estatura = res.getDouble("estatura");
                Double peso = res.getDouble("peso");
                String diagnostico = res.getString("diagnostico");
                ConsultaRutinaria consultaRutinaria = new ConsultaRutinaria(presionArterial, diagnostico, estatura, peso, id, fecha, sintomas, idPaciente);
                consultasRutinarias.add(consultaRutinaria);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return consultasRutinarias;
    }

    @Override
    public Modelo.Entity.ConsultaRutinaria selectId(Modelo.Entity.ConsultaRutinaria consultaRutinaria) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        ConsultaRutinaria registroConsultaRutinaria = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, consultaRutinaria.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            String fecha = res.getString("fecha");
            String sintomas = res.getString("sintomas");
            String idPaciente = res.getString("id_paciente");
            String presionArterial = res.getString("presion_arterial");
            Double estatura = res.getDouble("estatura");
            Double peso = res.getDouble("peso");
            String diagnostico = res.getString("diagnostico");
            registroConsultaRutinaria = new ConsultaRutinaria(presionArterial, diagnostico, estatura, peso, id, fecha, sintomas, idPaciente);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroConsultaRutinaria;
    }

    @Override
    public int update(Modelo.Entity.ConsultaRutinaria consultaRutinaria) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setInt(8, consultaRutinaria.getId());
            ps.setString(1, consultaRutinaria.getFecha());
            ps.setString(2, consultaRutinaria.getSintomas());
            ps.setString(3, consultaRutinaria.getIdPaciente());
            ps.setString(4, consultaRutinaria.getPresionArterial());
            ps.setDouble(5, consultaRutinaria.getEstatura());
            ps.setDouble(6, consultaRutinaria.getPeso());
            ps.setString(7, consultaRutinaria.getDiagnostico());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Modelo.Entity.ConsultaRutinaria consultaRutinaria) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, consultaRutinaria.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
