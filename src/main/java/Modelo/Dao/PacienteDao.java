/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.Paciente;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author estudiante
 */
public class PacienteDao implements PacienteServices {

    public static final String SQL_INSERT = "INSERT INTO paciente(id, nombre, apellido, direccion, telefono, correo_electronico, tipo_sangre, id_seguro) VALUES (?,?,?,?,?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM paciente";
    public static final String SQL_CONSULTAID = "SELECT * FROM paciente WHERE id = ?";
    public static final String SQL_DELETE = "DELETE FROM paciente WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE paciente SET nombre = ?, apellido = ?, direccion = ?, telefono = ?, correo_electronico = ?, tipo_sangre = ?, id_seguro = ? WHERE id = ?";

    @Override
    public int create(Modelo.Entity.Paciente paciente) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setString(1, paciente.getId());
            ps.setString(2, paciente.getNombre());
            ps.setString(3, paciente.getApellido());
            ps.setString(4, paciente.getDireccion());
            ps.setString(5, paciente.getTelefono());
            ps.setString(6, paciente.getCorreoElectronico());
            ps.setString(7, paciente.getTipoSangre());
            ps.setString(8, paciente.getIdSeguro());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Modelo.Entity.Paciente> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Paciente> pacientes = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                String id = res.getString("id");
                String nombre = res.getString("nombre");
                String apellido = res.getString("apellido");
                String direccion = res.getString("direccion");
                String telefono = res.getString("telefono");
                String correoElectronico = res.getString("correo_electronico");
                String tipoSangre = res.getString("tipo_sangre");
                String idSeguro = res.getString("id_seguro");
                Paciente paciente = new Paciente(tipoSangre, idSeguro, id, nombre, apellido, direccion, telefono, correoElectronico);
                pacientes.add(paciente);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return pacientes;
    }

    @Override
    public Modelo.Entity.Paciente selectId(Modelo.Entity.Paciente paciente) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Paciente registroPaciente = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setString(1, paciente.getId());
            res = ps.executeQuery();
            res.absolute(1);
            String id = res.getString("id");
            String nombre = res.getString("nombre");
            String apellido = res.getString("apellido");
            String direccion = res.getString("direccion");
            String telefono = res.getString("telefono");
            String correoElectronico = res.getString("correo_electronico");
            String tipoSangre = res.getString("tipo_sangre");
            String idSeguro = res.getString("id_seguro");
            registroPaciente = new Paciente(tipoSangre, idSeguro, id, nombre, apellido, direccion, telefono, correoElectronico);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroPaciente;
    }

    @Override
    public int update(Modelo.Entity.Paciente paciente) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(8, paciente.getId());
            ps.setString(1, paciente.getNombre());
            ps.setString(2, paciente.getApellido());
            ps.setString(3, paciente.getDireccion());
            ps.setString(4, paciente.getTelefono());
            ps.setString(5, paciente.getCorreoElectronico());
            ps.setString(6, paciente.getTipoSangre());
            ps.setString(7, paciente.getIdSeguro());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Modelo.Entity.Paciente paciente) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setString(1, paciente.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
