/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.Rellenable;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author estudiante
 */
public class RellenableDao implements RellenableServices {

    public static final String SQL_INSERT = "INSERT INTO rellenable(id, fecha, dosis, duracion, id_paciente, id_medicamento, numero_recargas, comentarios) VALUES (?,?,?,?,?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM rellenable";
    public static final String SQL_CONSULTAID = "SELECT * FROM rellenable WHERE id = ?";
    public static final String SQL_DELETE = "DELETE FROM rellenable WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE rellenable SET fecha = ?, dosis = ?, duracion = ?, id_paciente = ?, id_medicamento = ?, numero_recargas = ?, comentarios = ? WHERE id = ?";

    @Override
    public int create(Modelo.Entity.Rellenable rellenable) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, rellenable.getId());
            ps.setString(2, rellenable.getFecha());
            ps.setString(3, rellenable.getDosis());
            ps.setString(4, rellenable.getDuracion());
            ps.setString(5, rellenable.getIdPersona());
            ps.setInt(6, rellenable.getIdMedicamento());
            ps.setInt(7, rellenable.getNumeroRecargas());
            ps.setString(8, rellenable.getComentarios());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Modelo.Entity.Rellenable> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Rellenable> rellenables = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                String fecha = res.getString("fecha");
                String dosis = res.getString("dosis");
                String duracion = res.getString("duracion");
                String idPaciente = res.getString("id_paciente");
                int idMedicamento = res.getInt("id_medicamento");
                int numeroRecargas = res.getInt("numero_recargas");
                String comentarios = res.getString("id_medicamento");
                Rellenable rellenable = new Rellenable(comentarios, numeroRecargas, id, fecha, dosis, duracion, idMedicamento, idPaciente);
                rellenables.add(rellenable);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return rellenables;
    }

    @Override
    public Modelo.Entity.Rellenable selectId(Modelo.Entity.Rellenable rellenable) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Rellenable registroRellenable = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, rellenable.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            String fecha = res.getString("fecha");
            String dosis = res.getString("dosis");
            String duracion = res.getString("duracion");
            String idPaciente = res.getString("id_paciente");
            int idMedicamento = res.getInt("id_medicamento");
            int numeroRecargas = res.getInt("numero_recargas");
            String comentarios = res.getString("id_medicamento");
            registroRellenable = new Rellenable(comentarios, numeroRecargas, id, fecha, dosis, duracion, idMedicamento, idPaciente);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroRellenable;
    }

    @Override
    public int update(Modelo.Entity.Rellenable rellenable) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setInt(8, rellenable.getId());
            ps.setString(1, rellenable.getFecha());
            ps.setString(2, rellenable.getDosis());
            ps.setString(3, rellenable.getDuracion());
            ps.setString(4, rellenable.getIdPersona());
            ps.setInt(5, rellenable.getIdMedicamento());
            ps.setInt(6, rellenable.getNumeroRecargas());
            ps.setString(7, rellenable.getComentarios());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Modelo.Entity.Rellenable rellenable) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, rellenable.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
