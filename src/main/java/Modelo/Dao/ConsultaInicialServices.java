/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.ConsultaInicial;
import java.util.List;

/**
 *
 * @author estudiante
 */
public interface ConsultaInicialServices {

    public int create(ConsultaInicial consultaInicial);
    public List<ConsultaInicial> all();
    public ConsultaInicial selectId(ConsultaInicial consultaInicial);
    public int update(ConsultaInicial consultaInicial);
    public int delete(ConsultaInicial consultaInicial);
            
}
