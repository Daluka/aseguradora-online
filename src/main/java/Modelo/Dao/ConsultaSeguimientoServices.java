/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import java.util.List;
import Modelo.Entity.ConsultaSeguimiento;

/**
 *
 * @author estudiante
 */
public interface ConsultaSeguimientoServices{

    public int create(ConsultaSeguimiento consultaSeguimiento);
    public List<ConsultaSeguimiento> all();
    public ConsultaSeguimiento selectId(ConsultaSeguimiento consultaSeguimiento);
    public int update(ConsultaSeguimiento hospital);
    public int delete(ConsultaSeguimiento hospital);
}
