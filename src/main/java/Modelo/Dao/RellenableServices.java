/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.Rellenable;
import java.util.List;

/**
 *
 * @author estudiante
 */
public interface RellenableServices {

    public int create(Rellenable Rellenable);
    public List<Rellenable> all();
    public Rellenable selectId(Rellenable Rellenable);
    public int update(Rellenable Rellenable);
    public int delete(Rellenable Rellenable);
            
}
