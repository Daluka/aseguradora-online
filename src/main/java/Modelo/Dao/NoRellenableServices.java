/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.NoRellenable;
import java.util.List;

/**
 *
 * @author estudiante
 */
public interface NoRellenableServices {

    public int create(NoRellenable noRellenable);
    public List<NoRellenable> all();
    public NoRellenable selectId(NoRellenable noRellenable);
    public int update(NoRellenable noRellenable);
    public int delete(NoRellenable noRellenable);
            
}
