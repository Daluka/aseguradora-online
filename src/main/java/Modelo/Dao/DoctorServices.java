/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.Doctor;
import java.util.List;

/**
 *
 * @author estudiante
 */
public interface DoctorServices {

    public int create(Doctor doctor);
    public List<Doctor> all();
    public Doctor selectId(Doctor doctor);
    public int update(Doctor doctor);
    public int delete(Doctor doctor);
            
}
