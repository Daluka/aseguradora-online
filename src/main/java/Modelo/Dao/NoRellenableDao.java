/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.NoRellenable;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author estudiante
 */
public class NoRellenableDao implements NoRellenableServices {

    public static final String SQL_INSERT = "INSERT INTO no_rellenable(id, fecha, dosis, duracion, id_paciente, id_medicamento, razon) VALUES (?,?,?,?,?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM no_rellenable";
    public static final String SQL_CONSULTAID = "SELECT * FROM no_rellenable WHERE id = ?";
    public static final String SQL_DELETE = "DELETE FROM no_rellenable WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE no_rellenable SET fecha = ?, dosis = ?, duracion = ?, id_paciente = ?, id_medicamento = ?, razon = ? WHERE id = ?";

    @Override
    public int create(Modelo.Entity.NoRellenable noRellenable) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, noRellenable.getId());
            ps.setString(2, noRellenable.getFecha());
            ps.setString(3, noRellenable.getDosis());
            ps.setString(4, noRellenable.getDuracion());
            ps.setString(5, noRellenable.getIdPersona());
            ps.setInt(6, noRellenable.getIdMedicamento());
            ps.setString(7, noRellenable.getRazon());
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Modelo.Entity.NoRellenable> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<NoRellenable> noRellenables = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                String fecha = res.getString("fecha");
                String dosis = res.getString("dosis");
                String duracion = res.getString("duracion");
                String idPaciente = res.getString("id_paciente");
                int idMedicamento = res.getInt("id_medicamento");
                String razon = res.getString("razon");
                NoRellenable noRellenable = new NoRellenable(razon, id, fecha, dosis, duracion, idMedicamento, idPaciente);
                noRellenables.add(noRellenable);

            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return noRellenables;
    }

    @Override
    public Modelo.Entity.NoRellenable selectId(Modelo.Entity.NoRellenable noRellenable) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        NoRellenable registroNoRellenable = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, noRellenable.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            String fecha = res.getString("fecha");
            String dosis = res.getString("dosis");
            String duracion = res.getString("duracion");
            String idPaciente = res.getString("id_paciente");
            int idMedicamento = res.getInt("id_medicamento");
            String razon = res.getString("razon");
            registroNoRellenable = new NoRellenable(razon, id, fecha, dosis, duracion, idMedicamento, idPaciente);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroNoRellenable;
    }

    @Override
    public int update(Modelo.Entity.NoRellenable noRellenable) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setInt(7, noRellenable.getId());
            ps.setString(1, noRellenable.getFecha());
            ps.setString(2, noRellenable.getDosis());
            ps.setString(3, noRellenable.getDuracion());
            ps.setString(4, noRellenable.getIdPersona());
            ps.setInt(5, noRellenable.getIdMedicamento());
            ps.setString(6, noRellenable.getRazon());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Modelo.Entity.NoRellenable noRellenable) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, noRellenable.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
