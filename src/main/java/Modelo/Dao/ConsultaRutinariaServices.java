/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.ConsultaRutinaria;
import java.util.List;

/**
 *
 * @author estudiante
 */
public interface ConsultaRutinariaServices {

    public int create(ConsultaRutinaria consultaRutinaria);
    public List<ConsultaRutinaria> all();
    public ConsultaRutinaria selectId(ConsultaRutinaria ConsultaRutinaria);
    public int update(ConsultaRutinaria ConsultaRutinaria);
    public int delete(ConsultaRutinaria consultaRutinaria);
            
}
