/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Entity;

/**
 *
 * @author estudiante
 */
public class Prescripcion {

    protected int id;
    protected String fecha, dosis, duracion;
    protected int idMedicamento;
    protected String idPersona;

    public Prescripcion(int id, String fecha, String dosis, String duracion, int idMedicamento, String idPersona) {
        this.id = id;
        this.fecha = fecha;
        this.dosis = dosis;
        this.duracion = duracion;
        this.idMedicamento = idMedicamento;
        this.idPersona = idPersona;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public int getMedicamento() {
        return idMedicamento;
    }

    public void setMedicamento(int idMedicamento) {
        this.idMedicamento = idMedicamento;
    }  

    public String getPersona() {
        return idPersona;
    }

    public void setPersona(String idPersona) {
        this.idPersona = idPersona;
    }
}
