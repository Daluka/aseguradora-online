/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Entity;

/**
 *
 * @author estudiante
 */
public class ConsultaSeguimiento extends Consulta{
    private String estadoDiagnostico;

    public ConsultaSeguimiento(String estadoDiagnostico, int id, String fecha, String sintomas, String idPaciente) {
        super(id, fecha, sintomas, idPaciente);
        this.estadoDiagnostico = estadoDiagnostico;
    }

    public String getEstadoDiagnostico() {
        return estadoDiagnostico;
    }

    public void setEstadoDiagnostico(String estadoDiagnostico) {
        this.estadoDiagnostico = estadoDiagnostico;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getSintomas() {
        return sintomas;
    }

    public void setSintomas(String sintomas) {
        this.sintomas = sintomas;
    }

    public String getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(String idPaciente) {
        this.idPaciente = idPaciente;
    }
    
}
