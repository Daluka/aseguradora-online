/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Entity;

/**
 *
 * @author estudiante
 */
public class Rellenable extends Prescripcion{
    private String comentarios;
    private int numeroRecargas;

    public Rellenable(String comentarios, int numeroRecargas, int id, String fecha, String dosis, String duracion, int idMedicamento, String idPersona) {
        super(id, fecha, dosis, duracion, idMedicamento, idPersona);
        this.comentarios = comentarios;
        this.numeroRecargas = numeroRecargas;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public int getNumeroRecargas() {
        return numeroRecargas;
    }

    public void setNumeroRecargas(int numeroRecargas) {
        this.numeroRecargas = numeroRecargas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public int getIdMedicamento() {
        return idMedicamento;
    }

    public void setMedicamento(int idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public String getIdPersona() {
        return idPersona;
    }

    public void setPersona(String idPersona) {
        this.idPersona = idPersona;
    }
    
}
