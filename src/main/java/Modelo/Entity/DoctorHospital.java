/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author Daniel
 */
public class DoctorHospital {

    private String idDoctor, idHospital;

    public DoctorHospital(String idDoctor, String idHospital) {
        this.idDoctor = idDoctor;
        this.idHospital = idHospital;
    }

    public DoctorHospital() {
    }

    public String getIdDoctor() {
        return idDoctor;
    }

    public String getIdHospital() {
        return idHospital;
    }

    public void setIdDoctor(String idDoctor) {
        this.idDoctor = idDoctor;
    }

    public void setIdHospital(String idHospital) {
        this.idHospital = idHospital;
    }

}
