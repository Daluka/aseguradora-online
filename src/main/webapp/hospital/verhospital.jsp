<%-- 
    Document   : verhospital
    Created on : 16/06/2022, 9:02:17 a. m.
    Author     : estudiante
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    </head>

</head>
<body>
    <h1>Hospitales</h1>
    <a class="btn btn-primary" href="${pageContext.request.contextPath}/hospital/agregarhospital.jsp" role="button">Agregar</a>
    <table> 
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Direccion</th>
        </tr>
        <c:forEach var="hospital" items="${hospitales}">
            <tr>
                <td>${hospital.id}</td>
                <td>${hospital.nombre}</td>
                <td>${hospital.direccion}</td>
            </tr>
        </c:forEach>
    </table>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>
