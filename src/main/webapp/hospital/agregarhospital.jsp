<%-- 
    Document   : agregarhospital
    Created on : 23/06/2022, 08:43:35 AM
    Author     : estudiante
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    </head>
</head>
<body>
    <form action="${pageContext.request.contextPath}/hospital?accion=crear">
        <div class="mb-3">
            <label for="idHospital" class="form-label">Hospital</label>
            <input type="" class="form-control" id="id" name="Id">
        </div>
        <div class="mb-3">
            <label for="nombreHospital" class="form-label">Nombre</label>
            <input type="password" class="form-control" id="nombre" name="Nombre">
        </div>
        <div class="mb-3">
            <label for="direccionHospital" class="form-label">Direccion</label>
            <input type="password" class="form-control" id="direccion" name="Direccion">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>
